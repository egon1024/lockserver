# LockServer 0.9b - 7/14/2000

## What is LockServer?

Where I work, we have need of row locking for a database that doesn't
provide it.  The solution I came up with was to write a generic lock server that could provide the functionality we needed.

## Ok, so how does it work?
 The server itself is network based, and so far it seems to handle load
reasonably well.  Essentially, you make a request, it gets queued.  The 
server will return where you are in the queue.

## How does it REALLY work?
The storage mechanism is basically implemented as a hash of hashes of linked lists.  The protocol the lock server uses is described in the PROTOCOL document that should accompany this package.

## How do I use LockServer?
There are a couple of simple client libraries included in this package.  Pull requests for cleaner implementation or new languages will be looked on quite favorably.  Guidelines and technical information on the LockServer are available in the PROTOCOL document.


Thanks to Eric Johnson and Robert Holak for development ideas and testing.


