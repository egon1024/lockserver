<?
class LockConnect {
  var $DEBUG = 0;
  var $LockHost = "";
  var $LockPort = "";
  var $Conn = "";
  var $Status = "";
  var $Connected = 0;
  var $Estr = "";
  var $Eno = "";

  function LockConnect ($ls="",$lp="") {
    global $config;
    if ($ls != "") {
      $this->LockHost = $ls;
    } else {
      $config->LockHost ? $this->LockHost = $config->LockHost : $this->LockHost = "localhost";
    }
    if ($lp != "") {
      $this->LockPort = $lp;
    } else {
      $config->LockPort ? $this->LockPort = $config->LockPort : $this->LockPort = 8675;
    }
    $this->Connected = $this->LS_Connect();
  }

  function LS_Connect () {
    if ($this->DEBUG) print "=> <font color=orange size=+1>Inside LS_Connect()</font><br>\n";
    $this->Conn = fsockopen($this->LockHost,
                            $this->LockPort,&$eno,&$estr,10);
    if ($this->DEBUG) print "=> Ran fsockopen()<br>\n";
    if ($this->Conn) {
      $this->Status = "groovy";
      if ($this->DEBUG) print "=> Connected to lockserver<br>\n";
      if (!($connected = ord(fgetc($this->Conn)))) {
        if ($this->DEBUG) print "=> Failed fread()<br>\n";
        $this->Status = "noconn";
        $this->Estr[] .= "ERROR: Failed in Lock constructor.\n";
      } else {
        if ($connected == 0) {
          $this->Status = "badhost";
          $this->Estr[] .= "ERROR: Our IPaddr was rejected?\n";
          if ($this->DEBUG) {
            print "=> ($connected) Failed accept from lockserver<br>\n";
          }
        }
        if ($connected == 2) {
          $this->Status = "toobusy";
          $this->Estr[] .= "ERROR: lockserver is too busy\n";
          if ($this->DEBUG) {
            print "=> ($connected) Failed accept from lockserver<br>\n";
          }
        }
      }
    } else {
      if ($this->DEBUG) print "=> Couldn't connect<br>\n";
      $this->Status = "noconn";
      $this->Eno = $eno;
      $this->Estr[] .= "$estr\n";
    }
    if ($this->DEBUG) print "=> <font color=orange size=+1>Leaving LS_Connect()</font><br>\n";
    return $connected;
  }

  function LS_Close () {
    if ($this->DEBUG) print "=> <font color=orange size=+1>Entering LS_Close()</font><br>\n";
    if (!fclose($this->Conn)) {
      $this->Estr[] .= "ERROR: failed to close connection\n";
      if ($this->DEBUG) {
        print "=> <blink>Couldn't close connection!</blink><br>\n";
      }
    }
    $this->Conn = "";
    $this->Status = "noconn";
    if ($this->DEBUG) print "=> <font color=orange size=+1>Leaving LS_Close()</font><br>\n";
    return;
  }

  function ErrTrace () {
    print "<font color=red>\n";
    print "====> Status: $this->Status<br>\n";
    if (is_array($this->Estr)) {
      while (list($k,$v) = each($this->Estr)) {
        print "====> Trace: $v<br>\n";
      }
    } else {
      print "====> No errors to report<br>\n";
    }
    print "</font>\n";
    return;
  }
}

class LSLock {
  var $DEBUG = 0;
  var $Owner = "";
  var $Category = "";
  var $Index = "";
  var $Catlen = "";
  var $InLen = "";
  var $Conn = "";
  var $Status = "";
  var $Estr = "";

  function ErrTrace () {
    print "<font color=red>\n";
    print "====> Status: $this->Status<br>\n";
    if (is_array($this->Estr)) {
      while (list($k,$v) = each($this->Estr)) {
        print "====> Trace: $v<br>\n";
      }
    } else {
      print "====> No errors to report<br>\n";
    }
    print "</font>\n";
    return;
  }

  function LSLock ($co="",$o="",$c="",$i="") {
    if ($this->DEBUG) print "=> Inside LSLock ctor<br>\n";
    if ($co == "") {
      $conn = new LockConnect();
      if (!$conn->Connected) {
        $conn->ErrTrace();
        return "";
      }
      $this->Conn = $conn->Conn;
    } else {
      $this->Conn = $co;
    }
    $this->Status = "groovy";
    $this->Owner = $o;
    $this->Category = $c;
    $this->Index = $i;
    $this->Ownlen = strlen($o);
    $this->Catlen = strlen($c);
    $this->Inlen = strlen($i);
    if ($this->DEBUG) {
      print "<font color=#ffff22>\n";
      print "=> => owner = $this->Owner<br>\n";
      print "=> => category = $this->Category<br>\n";
      print "=> => index = $this->Index<br>\n";
      print "=> => ownlen = $this->Ownlen<br>\n";
      print "=> => catlen = $this->Catlen<br>\n";
      print "=> => inlen = $this->Inlen<br></font>\n";
    }
    if ($this->DEBUG) print "=> Leaving LSLock ctor<br>\n";
  }

  function InitLock ($co="",$o="",$c="",$i="") {
    if ($this->DEBUG) print "=> Inside LSLock initializer<br>\n";
    if ($co == "") {
      $conn = new LockConnect();
      if (!$conn->Connected) {
        $conn->ErrTrace();
        return "";
      }
      $this->Conn = $conn->Conn;
    } else {
      $this->Conn = $co;
    }
    $this->Status = "groovy";
    $this->Owner = $o;
    $this->Category = $c;
    $this->Index = $i;
    $this->Ownlen = strlen($o);
    $this->Catlen = strlen($c);
    $this->Inlen = strlen($i);
    if ($this->DEBUG) print "=> Leaving LSLock initializer<br>\n";
  }

  function StatsDump () {
    if ($this->DEBUG) print "=> Inside StatsDump()<br>\n";
    $commid = 0x08;
    $command = sprintf("%c",$commid);
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      if ($this->DEBUG) {
        print "=> Bailed StatsDump(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      $this->Estr[] .= "ERROR: StatsDump(fputs), no connection\n";
      return -1;
    }
    if ($this->DEBUG) print "=> Sent command<br>\n";
    $Q[Uptime] = 0; $Q[UnknownCommand] = 0; $Q[OwnerCommand] = 0;
    $Q[RequestLock] = 0;  $Q[QueueLocation] = 0; $Q[RenewLock] = 0;
    $Q[ReleaseLock] = 0; $Q[ReleaseAll] = 0; $Q[QueueDump] = 0;
    $Q[StatDump] = 0; $Q[TotalAccepts] = 0; $Q[TotalBad] = 0;
    $Q[TotalBusy] = 0; $Q[NumOwners] = 0; $Q[NumContenders] = 0;
    if ($this->DEBUG) print "<font color=seagreen>\n";
    while (list($k,$v) = each($Q)) {
      $b1 = ord(fgetc($this->Conn));
      $b2 = ord(fgetc($this->Conn));
      $b3 = ord(fgetc($this->Conn));
      $b4 = ord(fgetc($this->Conn));
      $n1 = (($b1 << 8) & 0xff00) | $b2;
      $n2 = (($b3 << 8) & 0xff00) | $b4;
      if ($this->DEBUG) print "=++> Grabbing 4B for $k<br>\n";
      $Q[$k] = ($n1 << 16) | $n2;
    }
    $b1 = ord(fgetc($this->Conn));
    $b2 = ord(fgetc($this->Conn));
    $num = (($b1 << 8) & 0xff00) | $b2;
    if ($this->DEBUG) print "=++> Grabbing 2B for CurrentConnects<br>\n";
    if ($this->DEBUG) print "</font>\n";
    $Q[CurrentConnects] = $num;
    if ($this->DEBUG) print "=> Leaving StatsDump()<br>\n";
    return $Q;
  }

  function QueueDump () {
    if ($this->DEBUG) print "=> Inside QueueDump()<br>\n";
    $commid = 0x07;
    $command = sprintf("%c%c%s",$commid,
               $this->Catlen,$this->Category);
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      if ($this->DEBUG) {
        print "=> Bailed QueueDump(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) print "=> Sent command<br>\n";
    $b1 = ord(fgetc($this->Conn));
    $b2 = ord(fgetc($this->Conn));
    $num = (($b1 << 8) & 0xff00) | $b2;
    if (!isset($num)) {
      if ($this->DEBUG) {
        print "=> Bailed QueueDump(fgetc), lost connection\n";
      }
      $this->Status = "noconn";
      return "";
    }
    for ($i=0; $i < $num; $i++) {
      $p = ord(fgetc($this->Conn));
      $b1 = ord(fgetc($this->Conn));
      $b2 = ord(fgetc($this->Conn));
      $ttl = (($b1 << 8) & 0xFF00) | $b2;
      $b1 = ord(fgetc($this->Conn));
      $b2 = ord(fgetc($this->Conn));
      $ttw = (($b1 << 8) & 0xFF00) | $b2;
      $si = ord(fgetc($this->Conn));
      $in = fread($this->Conn,$si);
      $so = ord(fgetc($this->Conn));
      $ow = fread($this->Conn,$so);
      $hs = ord(fgetc($this->Conn));
      $Q[$i]["TOTAL"] = $num;
      $Q[$i]["HasLock"] = $hs;
      $Q[$i]["Owner"] = $ow;
      $Q[$i]["Key"] = $this->Category;
      $Q[$i]["Index"] = $in;
      $Q[$i]["TTL"] = $ttl;
      $Q[$i]["TTW"] = $ttw;
      $Q[$i]["Priority"] = $p;
    }
    if ($this->DEBUG) print "=> Leaving QueueDump successfully<br>\n";
    if ($Q) {return $Q;}
    return "";
  }

  function RequestLock ($ttl="",$ttw="",$pr="") {
    global $config;
    if ($this->DEBUG) print "=> Inside RequestLock<br>\n";
	if ($ttl == "") $ttl = $config->LockTTL;
	if ($ttw == "") $ttw = $config->LockTTW;
	if ($pr == "") $pr = $config->LockPriority;
    $commid = 0x02;
    $ttl1 = ($ttl >> 8) & 0x00FF;
    $ttl2 = $ttl & 0x00FF;
    $ttw1 = ($ttw >> 8) & 0x00FF;
    $ttw2 = $ttw & 0x00FF;
    $command = sprintf("%c%c%s%c%s%c%s%c%c%c%c%c",$commid,
               $this->Catlen,$this->Category,
               $this->Inlen,$this->Index,
               $this->Ownlen,$this->Owner,
               $ttl1,$ttl2,$ttw1,$ttw2,$pr);

    if ($this->Status != "groovy") {
      $this->Estr[] .= "ERROR: RequestLock(), not connected\n";
      if ($this->DEBUG) {
        print "=> Bailing RequestLock, not connected<br>\n";
      }
      return -1;
    }
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      $this->Estr[] .= "ERROR: RequestLock(), failed on fputs()\n";
      if ($this->DEBUG) {
        print "=> Bailed RequestLock(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) print "=> Sent command<br>\n";
    $b1 = ord(fgetc($this->Conn));
    $b2 = ord(fgetc($this->Conn));
    $retval = (($b1 << 8) & 0xff00) | $b2;
    if (!isset($retval)) {
      $this->Estr[] .= "ERROR: RequestLock(), failed on fgetc()\n";
      if ($this->DEBUG) {
        print "=> Bailed RequestLock(fread), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($retval == 0) {
      $this->Estr[] .= "WARN: Lockserver wants to shutdown\n";
      if ($this->DEBUG) {
        print "=> Leaving RequestLock($retval), ";
        print "lockserver shutdown<br>\n";
      }
      $this->Status = "shutdown";
    }
    if ($this->DEBUG) print "=> Leaving RequestLock($retval)<br>\n";
    return $retval;
  }

  function QueueLocation () {
    if ($this->DEBUG) print "=> Entering QueueLocation<br>\n";
    $commid = 0x03;
    $command = sprintf("%c%c%s%c%s%c%s",$commid,
               $this->Catlen,$this->Category,
               $this->Inlen,$this->Index,
               $this->Ownlen,$this->Owner);
    if ($this->DEBUG) print "=> Sent command<br>\n";
    if ($this->Status != "groovy") {
      $this->Estr[] .= "ERROR: QueueLocation(), not connected\n";
      if ($this->DEBUG) {
        print "=> Bailed QueueLocation, not connected<br>\n";
      }
      return -1;
    }
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      $this->Estr[] .= "ERROR: QueueLocation(), failed on fputs\n";
      if ($this->DEBUG) {
        print "=> Bailed QueueLocation(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    $b1 = ord(fgetc($this->Conn));
    $b2 = ord(fgetc($this->Conn));
    $lockloc = (($b1 << 8) & 0xff00) | $b2;
    if (!isset($lockloc)) {
      $this->Estr[] .= "ERROR: QueueLocation(), failed on fgetc\n";
      if ($this->DEBUG) {
        print "=> Bailed QueueLocation(fgetc), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) {
      print "=> Leaving QueueLocation($lockloc) successfully<br>\n";
    }
    return $lockloc;
  }

  function RenewLock () {
    if ($this->DEBUG) print "=> Entering RenewLock<br>\n";
    $commid = 0x04;
    $command = sprintf("%c%c%s%c%s%c%s",$commid,
               $this->Catlen,$this->Category,
               $this->Inlen,$this->Index,
               $this->Ownlen,$this->Owner);
    if ($this->Status != "groovy") {
      $this->Estr[] .= "ERROR: RenewLock(), not connected\n";
      if ($this->DEBUG) {
        print "=> Bailed RenewLock, not connected<br>\n";
      }
      return -1;
    }
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      $this->Estr[] .= "ERROR: RenewLock(), failed on fputs()\n";
      if ($this->DEBUG) {
        print "=> Bailed RenewLock(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) print "=> Sent command<br>\n";
    $retval = ord(fgetc($this->Conn));
    if (!isset($retval)) {
      $this->Estr[] .= "ERROR: RenewLock(), failed on fgetc()\n";
      if ($this->DEBUG) {
        print "=> Bailed RenewLock(fgetc), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) {
      print "=> Leaving RenewLock($retval) successfully<br>\n";
    }
    return $retval;
  }

  function ReleaseLock () {
    if ($this->DEBUG) print "=> Entering ReleaseLock<br>\n";
    $commid = 0x05;
    if ($this->Status != "groovy") {
      $this->Estr[] .= "ERROR: ReleaseLock(), not connected\n";
      if ($this->DEBUG) {
        print "=> Bailed ReleaseLock, not connected<br>\n";
      }
      return -1;
    }
    if ($this->DEBUG) print "=> ReleaseLock()<br>\n";
    $command = sprintf("%c%c%s%c%s%c%s",$commid,
             $this->Catlen,$this->Category,
             $this->Inlen,$this->Index,
             $this->Ownlen,$this->Owner);
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      $this->Estr[] .= "ERROR: ReleaseLock(), failed on fputs()\n";
      if ($this->DEBUG) {
        print "=> Bailed ReleaseLock(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) print "=> Sent command<br>\n";
    $b1 = ord(fgetc($this->Conn));
    $b2 = ord(fgetc($this->Conn));
    $rem = (($b1 << 8) & 0xff00) | $b2;
    if (!isset($rem)) {
      $this->Estr[] .= "ERROR: ReleaseLock(), failed on fgetc()\n";
      if ($this->DEBUG) {
        print "=> Bailed ReleaseLock(fgetc), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) {
      print "=> Leaving ReleaseLock($rem) successfully<br>\n";
    }
    return $rem;
  }

  function LockOwner ($c="",$i="") {
    if ($this->DEBUG) print "=> Entering LockOwner<br>\n";
    $commid = 0x01;
    if ($c == "" && $i == "") {
      $command = sprintf("%c%c%s%c%s",$commid,
                 $this->Catlen,$this->Category,
                 $this->Inlen,$this->Index);
    } else {
      $command = sprintf("%c%c%s%c%s",$commid,
                 strlen($c),$c,
                 strlen($i),$i);
    }
    if ($this->Status != "groovy") {
      $this->Estr[] .= "ERROR: LockOwner(), not connected\n";
      if ($this->DEBUG) {
        print "=> Bailed LockOwner, not connected<br>\n";
      }
      return -1;
    }
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      $this->Estr[] .= "ERROR: LockOwner(), failed on fputs()\n";
      if ($this->DEBUG) {
        print "=> Bailed LockOwner(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) print "=> Sent command<br>\n";
    $ownlen = ord(fgetc($this->Conn));
    if (!isset($ownlen)) {
      $this->Estr[] .= "ERROR: LockOwner(), failed on fgetc()\n";
      if ($this->DEBUG) {
        print "=> Bailed LockOwner(fgetc), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($ownlen == 0) {
      if ($this->DEBUG) {
        print "=> Leaving LockOwner($ownlen - <font color=red>No Owner</font>) successfully<br>\n";
      }
      return 0;
    }
    if (!$lockowner = fread($this->Conn,$ownlen)) {
      $this->Estr[] .= "ERROR: LockOwner(), failed on fread()\n";
      if ($this->DEBUG) {
        print "=> Bailed LockOwner(fread), ($ownlen) lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) {
      print "=> Leaving LockOwner($ownlen - $lockowner) ";
      print "successfully<br>\n";
    }
    return $lockowner;
  }

  function ReleaseAll ($all=0) {
    if ($this->DEBUG) print "=> Entering ReleaseAll<br>\n";
    $commid = 0x06;
    if ($this->Status != "groovy") {
      $this->Estr[] .= "ERROR: ReleaseAll(), not connected\n";
      if ($this->DEBUG) {
        print "=> Bailed ReleaseAll, not connected<br>\n";
      }
      return -1;
    }
    if ($all) {
      $command = sprintf("%c%c%s",$commid,0x01,"*");
    } else {
      $command = sprintf("%c%c%s",$commid,
              $this->Ownlen,$this->Owner);
    }
    if ($this->DEBUG) print "=> Sent command<br>\n";
    if (fputs($this->Conn,$command,strlen($command)) < 0) {
      $this->Estr[] .= "ERROR: ReleaseAll(), failed on fputs()\n";
      if ($this->DEBUG) {
        print "=> Bailed ReleaseAll(fputs), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    $b1 = ord(fgetc($this->Conn));
    $b2 = ord(fgetc($this->Conn));
    $rem = (($b1 << 8) & 0xff00) | $b2;
    if (!isset($rem)) {
      $this->Estr[] .= "ERROR: ReleaseAll(), failed on fgetc()\n";
      if ($this->DEBUG) {
        print "=> Bailed ReleaseAll(fgetc), lost connection<br>\n";
      }
      $this->Status = "noconn";
      return -1;
    }
    if ($this->DEBUG) {
      print "=> Leaving ReleaseAll($rem) successfully<br>\n";
    }
    return $rem;
  }
}

class NonBlock_Lock extends LSLock {
  var $ttl;
  var $ttw;
  var $pr;

  function NonBlock_Lock ($cn,$o,$c,$i,$ttl="",$ttw="",$pr="") {
    $this->InitLock($cn,$o,$c,$i);
    $this->ttl = $ttl;
    $this->ttw = $ttw;
    $this->pr = $pr;
  }

  function Attempt ($tl="",$tw="",$p="") {
    if ($this->DEBUG) {
      print "=> Inside Attempt()<br>\n";
    }
    if ($tl == "") $tl = $this->ttl;
    if ($tw == "") $tw = $this->ttw;
    if ($p == "") $p = $this->pr;
    $qloc = $this->RequestLock($tl,$tw,$p);
    if ($this->DEBUG) {
      print "=> Made RequestLock call, got $qloc<br>\n";
    }
    if ($qloc == 0) {
      $msg = "WARN: NonBlock_Lock::Attempt(), lockserver ";
      $msg .= "shutting down\n";
      $this->Estr[] .= $msg;
      if ($this->DEBUG) print "=> Lockserver shutting down<br>\n";
      $this->Status = "shutdown";
      return -1;
    } elseif ($qloc == 1) {
      if ($this->DEBUG) print "=> Got the lock!<br>\n";
      return 1;
    } elseif ($qloc == -1) {
      $msg = "ERROR: NonBlock_Lock::Attempt(), lost connection\n";
      $this->Estr[] .= $msg;
      if ($this->DEBUG) print "=> Lost connection to lockserver<br>\n";
      return -1;
    } else {
      if ($this->DEBUG) print "=> Lock was previously queued, attempting removal<br>\n";
      if (($resp = $this->ReleaseLock()) == -1) {
        if ($this->DEBUG) print "=> Couldn't dequeue request!<br>\n";
        return -1;
      }
      if ($this->DEBUG) print "=> Request dequeued...<br>\n";
      return 0;
    }
    if ($this->DEBUG) {
      print "=> Leaving Attempt()<br>\n";
    }
  }
}

class Block_Lock extends LSLock {
  var $NumTries;
  var $MaxLive;
  var $USleepTime;
  var $ttl;
  var $ttw;
  var $pr;

  function Block_Lock ($cn,$o,$c,$i,$tl="",$tw="",$pr="",$nt="",$ml="",$us="") {
    global $config;
    $this->InitLock($cn,$o,$c,$i);
    $this->ttl = $tl;
    $this->ttw = $tw;
    $this->pr = $pr;
    $nt == "" ? $this->NumTries = $config->BLockNumTries : $this->NumTries = $nt;
    $ml == "" ? $this->MaxLive = $config->BLockMaxLive : $this->MaxLive = $ml;
    $us == "" ? $this->USleepTime = $config->BLockUSleep : $this->USleepTime = $us;
  }

  function Attempt ($tl="",$tw="",$p="") {
    if ($this->DEBUG) {
      print "=> Inside Attempt()<br>\n";
    }
    if ($tl == "") $tl = $this->ttl;
    if ($tw == "") $tw = $this->ttw;
    if ($p == "") $p = $this->pr;
    $qloc = $this->RequestLock($tl,$tw,$p);
    if ($this->DEBUG) {
      print "=> Made RequestLock call<br>\n";
    }
    if ($qloc == 0) {
      $msg = "WARN: NonBlock_Lock::Attempt(), lock request not found\n";
      $this->Estr[] .= $msg;
      if ($this->DEBUG) print "=> Lock request not found<br>\n";
      return 0;
    } elseif ($qloc == -1) {
      $msg = "ERROR: NonBlock_Lock::Attempt(), lost connection\n";
      $this->Estr[] .= $msg;
      if ($this->DEBUG) print "=> Lost connection to Lockserver<br>\n";
      return -1;
    } elseif ($qloc == 1) {
      if ($this->DEBUG) print "=> Got the lock!<br>\n";
      return 1;
    } else {
      if ($this->DEBUG) print "=> Request queued, blocking...<br>\n";
      for ($cnt=0,$sts=time();
          ($cnt < $this->NumTries) && (time() < ($sts+$this->MaxLive));
          $cnt++) {
        usleep($this->USleepTime);
        $qloc = $this->QueueLocation();
        if ($qloc == 0) {
          $msg = "WARN: NonBlock_Lock::Attempt(), lock request not found\n";
          $this->Estr[] .= $msg;
          if ($this->DEBUG) {
            print "=> Lock request not found<br>\n";
          }
          return 0;
        } 
        if ($qloc == -1) {
          $msg = "ERROR: NonBlock_Lock::Attempt(), lost connection\n";
          $this->Estr[] .= $msg;
          if ($this->DEBUG) {
            print "=> Lost connection to Lockserver<br>\n";
          }
          return -1;
        }
        if ($qloc == 1) {
          if ($this->DEBUG) {
            print "=> Got the lock!!<br>\n";
          }
          return 1;
        }
      }
      if ($this->DEBUG) print "=> Internal timeout, dequeueing<br>\n";
      $msg = "WARN: NonBlock_Lock::Attempt(), internal timeout\n";
      $this->Estr[] .= $msg;
      if (($resp = $this->ReleaseLock()) == -1) {
        $msg = "ERROR: NonBlock_Lock::Attempt(), couldn't dequeue ";
        $msg .= "lock request\n";
        $this->Estr[] .= $msg;
        if ($this->DEBUG) print "=> Couldn't dequeue request!<br>\n";
        return -1;
      }
      if ($this->DEBUG) print "=> Request dequeued...<br>\n";
      return 0;
    }
  }
}
?>
