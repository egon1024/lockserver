BASE = lockserver
DIR = ./src

${BASE}: Makefile ${DIR}/${BASE}.c
	gcc -o ${BASE} -O2 -Wall ${DIR}/${BASE}.c ${CFLAGS} ${LIBS} 

clean:
	@rm -f ${BASE} *.o core
	@echo Cleaned...
