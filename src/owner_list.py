"""
This module encapsulates a list of owners
"""

# Built in imports
import threading

# 3rd Party imports

# Lockserver imports
from owner_data import OwnerData


class OwnerList(object):
    """
    A class that encapsulates a list of owners
    """

    def __init__(self):
        """
        Initializer
        """
        self.__map = {}
        self.__mutex = threading.Lock()

    def string_lock_dump(self):
        """
        Gives a string representation of what the list looks like
        """
        retval = []
        for (owner, data) in self.__map.items():
            retval.append(owner + ":")
            retval += data.string_lock_dump()

        return retval

    def setdefault(self, owner):
        """
        Sets the default owner for the list.  Expects a string representation
        of an owner.
        """
        tmp = OwnerData(owner)
        return self.__map.setdefault(owner, tmp)

    def lock(self):
        """
        Lock the object.
        """
        self.__mutex.acquire()

    def unlock(self):
        """
        Unlock the object.
        """
        self.__mutex.release()

    def get_data(self, owner):
        """
        Get the list for the given owner.
        """
        return self.__map.get(owner, None)

    def get_owners(self):
        """
        Get all the owners of lists
        """
        return self.__map.keys()

    def private_unsafe_clean(self):
        """
        A thread *un* safe manner of cleaning out the object.  Specifically
        for use by a maid.
        """
        retval = 0
        for (key, data) in self.__map.items():
            if len(data) == 0:
                del self.__map[key]
                retval += 1

        return retval
