"""
THis module encapsulates code in charge of occassionally cleaning out
structures and some other housekeeping tasks
"""

# Built in imports
import os
import syslog
import threading
import time

# 3rd Party imports
from mx import DateTime

# Lockserver imports


class TooBusyError(Exception):
    """
    This exception is raised when the server is too busy servicing clients to
    allow the maid to do its work.
    """
    pass


class Maid(threading.Thread):
    """
    A housekeeping class that takes care of cleaning up many of the data
    structures, updating reporting, etc.
    """

    def __init__(self, master_keylist, accountant, owner_list, rost):
        """
        Initializer
        """
        self.__last_run = DateTime.now()
        self.__need_to_run = False

        self.__master_kl = master_keylist
        self.__accountant = accountant
        self.__owner_list = owner_list
        self.__rost = rost

        threading.Thread.__init__(self)

    def run(self):
        """
        The main execution routine for this thread.
        """
        trigger_file = "/tmp/.runmaid"
        stat_file = "/tmp/.lockstat"
        dump_file = "/tmp/.lockdump"
        owner_file = "/tmp/.ownerdump"

        while True:
            # Provide lock statistics, if appropriate
            if os.path.exists(stat_file) and \
            not os.path.getsize(stat_file) and \
            not os.path.islink(stat_file) and \
            os.access(stat_file, os.W_OK):
                file_h = open(stat_file, "w")
                data = self.__accountant.string_dump(self.__owner_list)
                if data:
                    file_h.write(data)
                    file_h.write("\n")
                file_h.close()

            # Provide lock dump, if appropriate
            if os.path.exists(dump_file) and \
            not os.path.getsize(dump_file) and \
            not os.path.islink(dump_file) and \
            os.access(dump_file, os.W_OK):
                file_h = open(dump_file, "w")
                data = "\n".join(self.__master_kl.string_lock_dump())
                if data:
                    file_h.write(data)
                    file_h.write("\n")
                file_h.close()

            # Provide owner dump, if appropriate
            if os.path.exists(owner_file) and \
            not os.path.getsize(owner_file) and \
            not os.path.islink(owner_file) and \
            os.access(owner_file, os.W_OK):
                file_h = open(owner_file, "w")
                data = "\n".join(self.__owner_list.string_lock_dump())
                if data:
                    file_h.write(data)
                    file_h.write("\n")
                file_h.close()

            # Run the Maid today?
            now = DateTime.now()
            if DateTime.Date(now.year, now.month, now.day) > self.__last_run \
            and now.hour >= 6:
                self.__need_to_run = True

            # Another test - if "trigger_file" exists, we should clean
            if os.path.exists(trigger_file):
                self.__need_to_run = True
                try:
                    os.unlink(trigger_file)
                except IOError:
                    pass

            if self.__need_to_run:
                self.__need_to_run = False
                try:
                    self.clean_structures()
                    self.__last_run = DateTime.now()
                except TooBusyError:
                    self.__need_to_run = True
                    syslog.syslog(syslog.LOG_WARNING,
                                   "Lockserver too active to clean")

            time.sleep(10)

    def clean_structures(self):
        """
        Go through all the data structures and clean out as appropriate
        """
        max_tries = 10

        self.__rost.ready_to_clean()

        tries = 0
        while tries < max_tries and self.__rost.get_active_count() > 0:
            time.sleep(1)
            tries += 1

        if self.__rost.get_active_count() > 0:
            self.__rost.done_cleaning()
            raise TooBusyError

        starttime = time.time()

        # Now we do the cleaning
        cleancount = self.__master_kl.private_unsafe_clean()
        cleancount += self.__owner_list.private_unsafe_clean()

        self.__rost.done_cleaning()
        run_time = time.time() - starttime

        syslog.syslog(
            syslog.LOG_NOTICE,
            "Maid took %f seconds to clean %d nodes" % (run_time, cleancount)
        )
