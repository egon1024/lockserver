"""
This code encapulates a way to keep track of what is going on within in the
server
"""

# Built in imports
import threading
import time

# 3rd Party imports

# Lockserver imports


class Roster(object):
    """
    The Roster class keeps track of what is going on within the server
    """

    def __init__(self):
        """
        Initializer
        """
        self.__mutex = threading.Lock()
        self.__active_count = 0
        self.__ready_to_clean = 0

    def register(self):
        """
        This function is called by a Butler when it wants to go do some work
        """
        while(1):
            self.__mutex.acquire()
            if self.__ready_to_clean:
                self.__mutex.release()
                time.sleep(2)
            else:
                self.__active_count += 1
                self.__mutex.release()
                break

    def unregister(self):
        """
        Called by a Butler having finished work
        """
        self.__mutex.acquire()
        self.__active_count -= 1
        self.__mutex.release()

    def ready_to_clean(self):
        """
        Sets the flag indicating that it is ok for the maid to enter and do
        their work
        """
        self.__mutex.acquire()
        self.__ready_to_clean = 1
        self.__mutex.release()

    def done_cleaning(self):
        """
        Called by a maid when cleaning is complete to indicate that it is
        alright for butlers to get back to work
        """
        self.__ready_to_clean = 0

    def get_active_count(self):
        """
        Returns the number of active butlers working in the server
        """
        return self.__active_count

    def get_clean_status(self):
        """
        Returns an indicator as to whether it is ok for a maid to enter and
        begin cleaning
        """
        return self.__ready_to_clean
