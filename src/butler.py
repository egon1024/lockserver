"""
This module encapsulates a class that include the logic for interpreting a
command from the network representation, and the logic for executing those
commands.
"""

# Built in imports
from struct import pack, unpack

# 3rd Party imports

# Lockserver imports
import contender


class DisconnectedError(Exception):
    """
    This exception is thrown when an attempt is made to work on a socket that
    has disconnected.
    """
    pass


class Butler(object):
    """
    This class encapsulates the interpretation of a command and the business
    logic for executing those commands
    """
    commands = {
        0x1: "WHOOWNS", 0x2: "GIMME", 0x3: "CONTLOC", 0x4: "RENEW",
        0x5: "RELSPEC", 0x6: "RELALL", 0x7: "SHOWCONT", 0x8: "STATS",
        "WHOOWNS": 0x1, "GIMME": 0x2, "CONTLOC": 0x3, "RENEW": 0x4,
        "RELSPEC": 0x5, "RELALL": 0x6, "SHOWCONT": 0x7, "STATS": 0x8,
    }

    def __init__(self, master_keylist, accountant, roster, owner_list):
        """
        Initialization method expects a KeyList and a Roster to be passed in
        """

        self.__master_keylist = master_keylist
        self.__accountant = accountant
        self.__owner_list = owner_list
        self.__roster = roster

    def get_command(self, data_stream):
        """
        Reads the command from the data_stream and interprets what command is
        being requested
        """
        try:
            command = data_stream.recv(1)
        except:
            raise DisconnectedError
        if not command:
            raise DisconnectedError
        command = int(unpack("!B", command)[0])

        return command

    def get_key(self, data_stream):
        """
        Reads the appropriate data from the stream to get a key
        """
        try:
            key_len = data_stream.recv(1)
        except:
            raise DisconnectedError
        if not key_len:
            raise DisconnectedError
        key_len = int(unpack("!B", key_len)[0])

        try:
            key = data_stream.recv(key_len)
        except:
            raise DisconnectedError
        if not key:
            raise DisconnectedError

        packed_key = "!{}s".format(key_len)
        key = unpack(packed_key, key)[0]

        return key

    def get_index(self, data_stream):
        """
        Reads the appropriate data from the stream to get an index
        """
        try:
            index_listen = data_stream.recv(1)
        except:
            raise DisconnectedError

        if not index_listen:
            raise DisconnectedError
        index_listen = int(unpack("!B", index_listen)[0])

        try:
            index = data_stream.recv(index_listen)
        except:
            raise DisconnectedError
        if not index:
            raise DisconnectedError

        packed_index = "!{}s".format(index_listen)
        index = unpack(packed_index, index)[0]

        return index

    def get_owner(self, data_stream):
        """
        Reads the appropriate data from the stream to get an owner
        """
        try:
            owner_len = data_stream.recv(1)
        except:
            raise DisconnectedError

        if not owner_len:
            raise DisconnectedError
        owner_len = int(unpack("!B", owner_len)[0])

        try:
            owner = data_stream.recv(owner_len)
        except:
            raise DisconnectedError

        if not owner:
            raise DisconnectedError
        packed_owner = "!{}s".format(owner_len)
        owner = ''.join(unpack(packed_owner, owner))

        return owner

    def get_ttl(self, data_stream):
        """
        Reads the appropriate data from the stream to get a ttl
        """
        try:
            ttl = data_stream.recv(2)
        except:
            raise DisconnectedError

        if not ttl:
            raise DisconnectedError
        ttl = int(unpack("!H", ttl)[0])

        return ttl

    def get_ttw(self, data_stream):
        """
        Reads the appropriate data from the stream to get a ttw
        """
        try:
            ttw = data_stream.recv(2)
        except:
            raise DisconnectedError

        if not ttw:
            raise DisconnectedError
        ttw = int(unpack("!H", ttw)[0])

        return ttw

    def get_priority(self, data_stream):
        """
        Reads the appropriate data from the stream to get a priority
        """
        try:
            priority = data_stream.recv(1)
        except:
            raise DisconnectedError

        if not priority:
            raise DisconnectedError
        priority = int(unpack("!B", priority)[0])

        return priority

    def execute_command(self, data_stream):
        """
        This is the main dispatch method for a butler.  It coordinates reading
        int the appropriate data off the stream and dispatching to the
        appropriate method to handle the requested command
        """
        # Get command from the data_stream
        command = self.get_command(data_stream)

        if command not in self.commands:
            self.__accountant.increment("unknown_commands")
            raise DisconnectedError("Unknown command: {}".format(command))

        # Make sure we have the number representation
        if type(command) == str:
            command = self.get_command[command]

        self.__roster.register()

        try:
            # Now we need send control to appropriate function
            if command == self.commands["WHOOWNS"]:
                self.who_owns(data_stream)

            elif command == self.commands["GIMME"]:
                self.gimme(data_stream)

            elif command == self.commands["CONTLOC"]:
                self.contender_location(data_stream)

            elif command == self.commands["RENEW"]:
                self.renew(data_stream)

            elif command == self.commands["RELSPEC"]:
                self.release_specific(data_stream)

            elif command == self.commands["RELALL"]:
                self.release_all(data_stream)

            elif command == self.commands["SHOWCONT"]:
                self.show_contenders(data_stream)

            elif command == self.commands["STATS"]:
                self.stats(data_stream)
        except:
            self.__roster.unregister()
            raise

        self.__roster.unregister()

    def who_owns(self, data_stream):
        """
        This encapsulates the logic for looking up who owns a lock
        """
        self.__accountant.increment("owner_lookups")
        key = self.get_key(data_stream)
        index = self.get_index(data_stream)

        continue_flag = 1
        ret_val = None

        # Do we have the key in the master key list?
        if continue_flag:
            self.__master_keylist.lock()
            index_list = self.__master_keylist.find(key)
            if index_list is None:
                continue_flag = 0
            self.__master_keylist.unlock()

        # Do we have the index in the index list?
        if continue_flag:
            index_list.lock()
            contender_list = index_list.find(index)
            if contender_list is None:
                continue_flag = 0
            index_list.unlock()

        # What about the Contender?
        if continue_flag:
            contender_list.lock()
            list_owner = contender_list.get_owner()

            contender_list.unlock()

            if list_owner is not None:
                ret_val = list_owner.get_owner()

        if ret_val is not None:
            packstring = "!" + str(len(ret_val)) + "s"
            data_stream.send(pack("!B", len(ret_val)))
            data_stream.send(pack(packstring, ret_val))
        else:
            data_stream.send(pack("!B", 0))

    def gimme(self, data_stream):
        """
        This encapsulates the logic for requesting a lock
        """
        self.__accountant.increment("lock_requests")
        key = self.get_key(data_stream)
        index = self.get_index(data_stream)
        owner = self.get_owner(data_stream)
        ttl = self.get_ttl(data_stream)
        ttw = self.get_ttw(data_stream)
        priority = self.get_priority(data_stream)

        self.__master_keylist.lock()
        index_list = self.__master_keylist.setdefault(key)
        self.__master_keylist.unlock()

        index_list.lock()
        contender_list = index_list.setdefault(index)
        index_list.unlock()

        # Add the Contender
        cont = contender.Contender(
            key=key, index=index, owner=owner, ttl=ttl, ttw=ttw,
            priority=priority
        )
        contender_list.lock()
        ret_val = contender_list.add(cont)
        contender_list.unlock()

        data_stream.send(pack("!H", ret_val & 0xffff))

    def contender_location(self, data_stream):
        """
        This encapsulates the logic for looking up where in a lock queue a
        Contender exists
        """
        self.__accountant.increment("contender_lookups")
        key = self.get_key(data_stream)
        index = self.get_index(data_stream)
        owner = self.get_owner(data_stream)

        # Do we have the key in the master key list?
        self.__master_keylist.lock()
        index_list = self.__master_keylist.setdefault(key)
        self.__master_keylist.unlock()

        # Do we have the index in the index list?
        index_list.lock()
        contender_list = index_list.setdefault(index)
        index_list.unlock()

        # Where is this particular Contender?
        cont = contender.Contender(
            key=key, index=index, owner=owner
        )
        contender_list.lock()
        ret_val = contender_list.get_position(cont, add=1)
        contender_list.unlock()

        data_stream.send(pack("!H", ret_val & 0xffff))

    def renew(self, data_stream):
        """
        This encapsulates the logic for renewing a previously requested lock
        """
        self.__accountant.increment("lock_renewals")
        key = self.get_key(data_stream)
        index = self.get_index(data_stream)
        owner = self.get_owner(data_stream)

        continue_flag = 1
        ret_val = 0

        # Do we have the key in the master key list?
        self.__master_keylist.lock()
        index_list = self.__master_keylist.find(key)
        if index_list is None:
            continue_flag = 0
        self.__master_keylist.unlock()

        # Do we have the index in the index list?
        if continue_flag:
            index_list.lock()
            contender_list = index_list.find(index)
            if contender_list is None:
                continue_flag = 0
            index_list.unlock()

        # Where is this particular Contender?
        if continue_flag:
            cont = contender.Contender(key=key, index=index, owner=owner)
            contender_list.lock()
            ret_val = contender_list.renew(cont)
            contender_list.unlock()

        data_stream.send(pack("!H", ret_val & 0xffff))

    def release_specific(self, data_stream):
        """
        This encapsulates the logic for releasing a specific lock
        """
        self.__accountant.increment("specific_releases")
        key = self.get_key(data_stream)
        index = self.get_index(data_stream)
        owner = self.get_owner(data_stream)

        ret_val = 0

        if index != '*':
            ret_val = self.__kill_contender(key, index, owner)

        # The index is a '*', do the search through the owner list
        else:
            self.__owner_list.lock()
            data = self.__owner_list.get_data(owner)
            self.__owner_list.unlock()

            continue_flag = 1

            if data is None:
                continue_flag = 0

            if continue_flag:
                data.lock()
                cont_list = data.get_ist()
                data.unlock()

                for item in cont_list:
                    if item.get_key() == key:
                        ret_val += self.__kill_contender(
                            item.get_key(),
                            item.get_index(),
                            item.get_owner())

        data_stream.send(pack("!H", ret_val & 0xffff))

    def release_all(self, data_stream):
        """
        This encapsulates the logic for releasing all of the requests for a
        specific owner
        """
        self.__accountant.increment("general_releases")
        owner = self.get_owner(data_stream)

        self.__owner_list.lock()
        data = self.__owner_list.get_data(owner)
        self.__owner_list.unlock()

        continue_flag = 1
        ret_val = 0

        if data is None:
            continue_flag = 0

        if continue_flag:
            data.lock()
            cont_list = data.get_ist()
            data.unlock()

            for item in cont_list:
                ret_val += self.__kill_contender(
                    item.get_key(),
                    item.get_index(),
                    item.get_owner()
                )

        data_stream.send(pack("!H", ret_val & 0xffff))

    def show_contenders(self, data_stream):
        """
        This encapsulates the logic for requesting a list of contenders
        """
        self.__accountant.increment("lock_dumps")
        key = self.get_key(data_stream)

        self.__master_keylist.lock()
        index_list = self.__master_keylist.find(key)
        self.__master_keylist.unlock()

        if index_list is None:
            data_stream.send(pack("!H", 0))
            return

        index_list.lock()

        total_contenders = 0
        contender_packed_data = []
        for contender_list in index_list.get_ist():
            contender_list.lock()
            val1, val2 = contender_list.stream_out()
            total_contenders += val1
            contender_packed_data.append(val2)
            contender_list.unlock()

        index_list.unlock()

        data_stream.send(pack("!H", total_contenders))
        data_stream.send(''.join(contender_packed_data))

    def stats(self, data_stream):
        """
        This encapsulates the logic for requesting stats data
        """
        self.__accountant.increment("stat_requests")
        statdump = self.__accountant.stat_dump(self.__owner_list)
        data_stream.send(statdump)

    def __kill_contender(self, key, index, owner):
        """
        This encapsulates the logic for removing a Contender
        """
        continue_flag = 1
        ret_val = 0

        # Do we have the key in the master key list?
        self.__master_keylist.lock()
        index_list = self.__master_keylist.find(key)
        if index_list is None:
            continue_flag = 0
        self.__master_keylist.unlock()

        # Do we have the index in the index list?
        if continue_flag:
            index_list.lock()
            contender_list = index_list.find(index)
            if contender_list is None:
                continue_flag = 0
            index_list.unlock()

        # Where is this particular Contender?
        if continue_flag:
            cont = contender.Contender(key=key, index=index, owner=owner)
            contender_list.lock()
            ret_val = contender_list.release(cont)
            contender_list.unlock()

        return ret_val
