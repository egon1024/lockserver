"""
This module contains code to represent a list of contenders for a specific lock
"""

# Built in imports
import threading

# 3rd Party imports

# Lockserver imports


class ContenderList(object):
    """
    A class to represent a list of contenders
    """
    def __init__(self, owner_list=None):
        """
        Initializer.  Expects an ownerlist
        """
        self.__mutex = threading.Lock()
        self.__contenders = []
        self.__owner_list = owner_list

    def __len__(self):
        """
        Determine the length of the list
        """
        self.__expire_stuff()
        return len(self.__contenders)

    def lock(self):
        """
        Obtain a lock on this list
        """
        self.__mutex.acquire()

    def unlock(self):
        """
        Release a lock on this list
        """
        self.__mutex.release()

    def get_owner(self):
        """
        Find the owner of this specific list
        """
        if len(self) == 0:
            return None
        else:
            return self.__contenders[0]

    def add(self, contender):
        """
        Add a contender to this list
        """
        self.__expire_stuff()

        self.__owner_list.lock()
        own_data = self.__owner_list.setdefault(contender.get_owner())
        self.__owner_list.unlock()
        own_data.lock()

        # Handle the case where there are no pre-existing contenders.
        # In this event, just add the Contender as the owner.
        #
        if not self.__contenders:
            own_data.add(contender)
            self.__contenders.append(contender)
            own_data.unlock()
            contender.make_owner()
            return 1

        # If the Contender already exists as the owner, just refresh it.
        #
        elif self.__contenders[0] == contender:
            item = self.__contenders[0]
            own_data.unlock()
            item.refresh()
            return 1

        # If none of the above cases match the current situation, iterate
        # through all the non-owning contenders and find out (1) if the
        # Contender already exists in the list, or (2) where the new
        # Contender should be inserted.
        #
        else:
            new_loc = 1
            for i in xrange(1, len(self.__contenders)):
                item = self.__contenders[i]
                if item == contender:
                    own_data.unlock()
                    item.refresh()
                    return i + 1

                if item.get_priority() <= contender.get_priority():
                    new_loc = i + 1

            own_data.add(contender)
            self.__contenders.insert(new_loc, contender)
            own_data.unlock()

            return new_loc + 1

    def get_position(self, contender, add=0):
        """
        Find the current position of the specified contender
        """
        if add:
            return self.add(contender)

        self.__expire_stuff()

        for i in xrange(len(self.__contenders)):
            item = self.__contenders[i]
            if item == contender:
                return i + 1

    def renew(self, contender):
        """
        Renew the request for a specific contender, allowing it to either wait
        to become the owner longer, or remain as the owner for a longer period
        """
        self.__expire_stuff()

        retval = 0
        for item in self.__contenders:
            if item == contender:
                item.refresh()
                retval = 1
                break

        return retval

    def release(self, contender):
        """
        Release the request for the lock
        """
        self.__owner_list.lock()
        own_data = self.__owner_list.setdefault(contender.get_owner())
        self.__owner_list.unlock()

        retval = 0
        for i in xrange(len(self.__contenders)):
            item = self.__contenders[i]
            if item == contender:
                retval = 1
                own_data.lock()
                own_data.remove(contender)
                del self.__contenders[i]
                own_data.unlock()
                break

        self.__expire_stuff()

        return retval

    def stream_out(self):
        """
        Return a tuple of data suitable for outputting to a stream
        """
        self.__expire_stuff()

        retval = []

        how_many = len(self.__contenders)
        for item in self.__contenders:
            retval.append(item.stream_out())

        return (how_many, ''.join(retval))

    def __expire_stuff(self):
        """
        Find any and all contenders that should be expired, and remove them
        """
        retval = 0

        for index in reversed(xrange(len(self.__contenders))):
            item = self.__contenders[index]
            if item.deletable():
                self.__owner_list.lock()
                own_data = self.__owner_list.setdefault(item.get_owner())
                self.__owner_list.unlock()
                own_data.lock()
                own_data.remove(item)
                del self.__contenders[index]
                own_data.unlock()
                retval += 1

        if self.__contenders:
            self.__contenders[0].make_owner()

        return retval

    # This is mostly like __expire_stuff, but without the locking as it
    # only gets called by the Maid
    def private_unsafe_clean(self):
        """
        The logic here is mostly the same as __expire_stuff, but does not
        contain locking and is intended for the explicit use of the Maid.
        """
        retval = 0

        for index in reversed(xrange(len(self.__contenders))):
            item = self.__contenders[index]
            if item.deletable():
                own_data = self.__owner_list.setdefault(item.get_owner())
                own_data.remove(item)
                del self.__contenders[index]
                retval += 1

        if self.__contenders:
            self.__contenders[0].make_owner()

        return retval

    def string_lock_dump(self):
        """
        Gives a string representation of what the list looks like
        """
        retval = []
        for item in self.__contenders:
            ret_str = "        "
            ret_str += item.get_owner()
            if item.is_owner():
                ret_str += " *"
            retval.append(ret_str)

        return retval
