"""
This module aids in turning the calling process into a backgrounded daemon
"""

import os


def daemonize():
    """
    Turns the calling process into a daemon
    """

    # First fork
    try:
        pid = os.fork()

    except OSError, exc:
        raise Exception("%s [%d]" % (exc.strerror, exc.errno))

    # Now we have the parent process exit
    if pid:
        os._exit(0)

    # New session/process group
    os.setsid()

    # Second fork
    try:
        pid = os.fork()

    except OSError, exc:
        raise Exception("%s [%d]" % (exc.strerror, exc.errno))

    if pid:
        os._exit(0)

    # Close fds and redirect "main" ones to /dev/null
    reset_file_descriptors()


def reset_file_descriptors():
    """
    Closes all file descriptors, and opens stderr/stdout/stdin to /dev/null
    """

    if (hasattr(os, "devnull")):
        dev_null = os.devnull
    else:
        dev_null = '/dev/null'

    # Set up standard file descriptors, pointing at /dev/null so we don't
    # upset them
    os.open(dev_null, os.O_RDWR)
    os.dup2(0, 1)
    os.dup2(0, 2)
