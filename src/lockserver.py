#!/usr/bin/env python

"""
This is the main executable
"""

# Built in imports
from argparse import ArgumentParser
from select import select

import socket
import syslog
import time

# 3rd party imports

# Lockserver imports
from client_handler import ClientHandler
from daemonize import daemonize
from ownerlist import OwnerList
from roster import roster

import accountant
import keyList
import maid


def main():
    """
    Main dispatch routine
    """
    syslog.openlog('lockserver.py', syslog.LOG_PID, syslog.LOG_LOCAL1)

    cli_args = read_cli_args()
    if cli_args.daemonize():
        daemonize()
    listen_sockets = create_listen_sockets(cli_args)
    run_server(cli_args, listen_sockets)


def read_cli_args():
    """
    Parse the arguments defined at the command line
    """

    usage = "Use --help for usage information"

    parser = ArgumentParser(usage=usage)

    parser.add_argument(
        '-d', '--daemonize',
        action='store_true',
        dest='daemonize',
        help='Run as a backgrounded daemon',
    )

    parser.add_argument(
        '-p', '--port',
        action='store',
        type=int,
        dest='port',
        help='The port for the server to listen on',
    )

    parser.add_argument(
        '--max-conn-queue',
        action='store',
        type=int,
        dest='max_conn_queue',
        help='Size of the connection queue',
    )

    parser.set_defaults(
        daemonize=False,
        max_conn_queue=500,
        port=8675,
    )

    cli_args = parser.parse_args()

    return cli_args


def create_listen_sockets(cli_args):
    """
    This function creates any appropriate listening sockets and returns them as
    a list
    """

    host = ''
    readsocks = []

    portsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    portsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    bound = 0
    while not bound:
        try:
            portsock.bind((host, cli_args.port))

        except socket.error:
            syslog.syslog(syslog.LOG_WARNING, "Waiting to bind.")
            time.sleep(2)

        else:
            syslog.syslog(
                syslog.LOG_INFO, "Bound to port {}".format(cli_args.port)
            )
            bound = 1

    portsock.listen(cli_args.max_conn_queue)

    readsocks.append(portsock)

    return readsocks


def run_server(cli_args, readsocks):
    """
    Event loop that accepts client connections and dispatches them to handlers
    """

    owner_list = OwnerList()

    master_keylist = keyList.keyList(owner_list=owner_list)
    lew = accountant.Accountant()

    rost = roster()

    maid_worker = maid.maid(
        masterKL=master_keylist, ac=lew, owner_list=owner_list, rost=rost
    )
    maid_worker.start()

    while True:
        readables = select(readsocks, [], [])[0]
        for sockobj in readables:
            newsock = sockobj.accept()[0]
            lew.increment("accepted_connections")
            handler = ClientHandler(
                master_keylist=master_keylist, client_socket=newsock, accountant=lew,
                roster=rost, owner_list=owner_list
            )
            handler.start()

if __name__ == '__main__':
    main()
