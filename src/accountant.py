"""
This module contains a class used for keeping some counters for the application
"""

# Built in imports
from struct import pack

import threading
import time

# 3rd Party imports

# Lockserver imports


class Accountant(object):
    """
    This class encapsulates the counters and interface to those counters.  If
    the interface is consistently used, an instance of this class should be
    threadsafe.
    """
    def __init__(self, active=True):
        """
        Initializer.  Sets initial counts.

        If the active argument is set to False, counts will not be kept
        """
        self.start_ts = time.time()
        self.lock_requests = 0
        self.unknown_commands = 0
        self.owner_lookups = 0
        self.contender_lookups = 0
        self.lock_renewals = 0
        self.specific_releases = 0
        self.general_releases = 0
        self.lock_dumps = 0
        self.stat_requests = 0

        self.accepted_connections = 0
        self.disallowed_connections = 0
        self.too_busy_connections = 0

        self.active_connections = 0

        self.active = active

        self.mutex = threading.Lock()

    def increment(self, item):
        """
        Increments a given count in a threadsafe manner.  The item argument
        should be the string value representing the counter to increment.
        """
        if not self.active:
            return

        if getattr(self, item, None) is None:
            return None

        self.mutex.acquire()
        setattr(self, item, getattr(self, item) + 1)
        self.mutex.release()

        return

    def decrement(self, item):
        """
        Increments a given count in a threadsafe manner.  The item argument
        should be the string value representing the counter to increment.
        """
        if not self.active:
            return

        if item != "active_connections":
            return

        self.mutex.acquire()
        setattr(self, item, getattr(self, item) - 1)
        self.mutex.release()

        return

    def get_stats(self, owner_list):
        """
        Retrieves all stats and provides it back as a list of values
        """
        retval = []

        # 1) uptime
        retval.append(int(time.time() - self.start_ts))

        # 2) Unknown commands
        retval.append(self.unknown_commands)

        # 3) Owner Lookups
        retval.append(self.owner_lookups)

        # 4) Lock requests
        retval.append(self.lock_requests)

        # 5) Contender Lookups
        retval.append(self.contender_lookups)

        # 6) Lock renewal requests
        retval.append(self.lock_renewals)

        # 7) Specific lock releases
        retval.append(self.specific_releases)

        # 8) General lock releases
        retval.append(self.general_releases)

        # 9) Lock dump requests
        retval.append(self.lock_dumps)

        # 10) Statistics requests
        retval.append(self.stat_requests)

        # 11) Accepted connections
        retval.append(self.accepted_connections)

        # 12) Connections rejected due to not being in the hosts file
        retval.append(self.disallowed_connections)

        # 13) Connections rejected due to the server being too busy
        retval.append(self.too_busy_connections)

        # Calculate number of owner/nonowner contenders
        owners = 0
        contenders = 0

        owner_list.lock()
        owners_list = owner_list.get_owners()
        owner_list.unlock()

        for owner in owners_list:
            owner_list.lock()
            data = owner_list.get_data(owner)
            owner_list.unlock()

            if data is None:
                continue

            data.lock()
            lock_list = data.get_ist()
            data.unlock()

            for lock in lock_list:
                contenders += 1
                owners += lock.is_owner()

        # 14) Number of current lock owners
        retval.append(owners)

        # 15) Number of (non owner) contenders
        retval.append(contenders)

        # 16) active connections
        retval.append(self.active_connections)

        return retval

    def stat_dump(self, owner_list):
        """
        Retrieves all counters and returns it in a format suitable for
        transmitting over the net
        """
        retval = []

        four_byte_mask = 0xffffffff

        part = self.get_stats(owner_list)

        # 1) uptime
        retval.append(pack("!I", part[0] & four_byte_mask))

        # 2) Unknown commands
        retval.append(pack("!I", part[1] & four_byte_mask))

        # 3) Owner Lookups
        retval.append(pack("!I", part[2] & four_byte_mask))

        # 4) Lock requests
        retval.append(pack("!I", part[3] & four_byte_mask))

        # 5) Contender Lookups
        retval.append(pack("!I", part[4] & four_byte_mask))

        # 6) Lock renewal requests
        retval.append(pack("!I", part[5] & four_byte_mask))

        # 7) Specific lock releases
        retval.append(pack("!I", part[6] & four_byte_mask))

        # 8) General lock releases
        retval.append(pack("!I", part[7] & four_byte_mask))

        # 9) Lock dump requests
        retval.append(pack("!I", part[8] & four_byte_mask))

        # 10) Statistics requests
        retval.append(pack("!I", part[9] & four_byte_mask))

        # 11) Accepted connections
        retval.append(pack("!I", part[10] & four_byte_mask))

        # 12) Connections rejected due to not being in the hosts file
        retval.append(pack("!I", part[11] & four_byte_mask))

        # 13) Connections rejected due to the server being too busy
        retval.append(pack("!I", part[12] & four_byte_mask))

        # 14) Number of current lock owners
        retval.append(pack("!I", part[13] & four_byte_mask))

        # 15) Number of (non owner) contenders
        retval.append(pack("!I", part[14] & four_byte_mask))

        # 16) active connections
        retval.append(pack("!H", part[15] & 0xffff))

        return ''.join(retval)

    def string_dump(self, owner_list):
        """
        Retrives counters and provides it back as a string suitable for
        printing or logging
        """

        retval = []

        part = self.get_stats(owner_list)

        # 1) uptime
        retval.append("Uptime: %s" % str(part[0]))

        # 2) Unknown commands
        retval.append("Unknown commands: %s" % str(part[1]))

        # 3) Owner Lookups
        retval.append("Owner lookups: %s" % str(part[2]))

        # 4) Lock requests
        retval.append("Lock requests: %s" % str(part[3]))

        # 5) Contender Lookups
        retval.append("Contender lookups: %s" % str(part[4]))

        # 6) Lock renewal requests
        retval.append("Lock renewals: %s" % str(part[5]))

        # 7) Specific lock releases
        retval.append("Specific lock releases: %s" % str(part[6]))

        # 8) General lock releases
        retval.append("General lock releases: %s" % str(part[7]))

        # 9) Lock dump requests
        retval.append("Lock dump requests: %s" % str(part[8]))

        # 10) Statistics requests
        retval.append("Statistic requests: %s" % str(part[9]))

        # 11) Accepted connections
        retval.append("Accepted connections: %s" % str(part[10]))

        # 12) Connections rejected due to not being in the hosts file
        retval.append("Rejected hosts: %s" % str(part[11]))

        # 13) Connections rejected due to the server being too busy
        retval.append("Too busy rejections: %s" % str(part[12]))

        # 14) Number of current lock owners
        retval.append("Current lock owners: %s" % str(part[13]))

        # 15) Number of (non owner) contenders
        retval.append("Total contenders: %s" % str(part[14]))

        # 16) active connections
        retval.append("Active connections: %s" % str(part[15]))

        return "\n".join(retval)
