"""
This module encapsulates the management of key based lists
"""

# Built in imports
import threading

# 3rd Party imports

# Lockserver imports
from index_list import IndexList


class KeyList(object):
    """
    This class encapsulates the management of key based lists
    """
    def __init__(self, owner_list=None):
        """
        Initializer
        """

        self.__mutex = threading.Lock()
        self.__index_lists = {}
        self.__owner_list = owner_list

    def find(self, key):
        """
        Find an IndexList based off a particular key
        """

        return self.__index_lists.get(key, None)

    def lock(self):
        """
        Lock the instance
        """
        self.__mutex.acquire()

    def unlock(self):
        """
        Unlock the instance
        """
        self.__mutex.release()

    def setdefault(self, key):
        """
        Set a default index list
        """
        tmp = IndexList(self.__owner_list)
        return self.__index_lists.setdefault(key, tmp)

    def private_unsafe_clean(self):
        """
        A method for performing a clean that is *NOT* thread safe.  Intended
        for the explicit use of a Maid.
        """
        retval = 0

        for (key, index) in self.__index_lists.items():
            retval += index.private_unsafe_clean()
            if len(index) == 0:
                retval += 1
                del self.__index_lists[key]

        return retval

    def string_lock_dump(self):
        """
        Gives a string representation of what the list looks like
        """
        retval = []

        for (key, index) in self.__index_lists.items():
            retval.append(key + ":")
            retval += index.string_lock_dump()

        return retval

    def __len__(self):
        """
        Return the length
        """
        return len(self.__index_lists)
