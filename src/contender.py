"""
This module contains a class that represents a contender for a lock
"""

# Built in imports
from struct import pack
import time


class Contender(object):
    """
    This class represents a request for a lock
    """

    DEFAULT_PRIORITY = 3
    DEFAULT_TTL = 300
    DEFAULT_TTW = 300

    def __init__(
        self, key, index, owner, priority=None, ttl=None, ttw=None
    ):
        """
        Initializer function.  Expects at least the key, index, and owner
        arguments.
        """
        self.__key = str(key)
        self.__index = str(index)
        self.__owner = str(owner)

        if priority is None:
            self.__priority = self.DEFAULT_PRIORITY
        else:
            self.__priority = int(priority)

        if ttl is None:
            self.__ttl = self.DEFAULT_TTL
        else:
            self.__ttl = int(ttl)

        if ttw is None:
            self.__ttw = self.DEFAULT_TTW
        else:
            self.__ttw = int(ttw)

        self.__am_owner = 0
        self.__obtain_ts = 0
        self.__request_ts = time.time()
        self.__last_update_ts = time.time()

    def __hash__(self):
        """
        Generate a hash key based on owner, key, and index
        """
        return hash((self.__owner, self.__key, self.__index))

    def get_key(self):
        """
        Return the key being requested
        """
        return self.__key

    def get_index(self):
        """
        Return the index being requested
        """
        return self.__index

    def get_owner(self):
        """
        Return the owner being requested
        """
        return self.__owner

    def get_priority(self):
        """
        Return the priority being requested
        """
        return self.__priority

    def get_ttw(self):
        """
        Return the ttw being requested
        """
        return self.__ttw

    def get_ttl(self):
        """
        Return the ttl being requested
        """
        return self.__ttl

    def refresh(self):
        """
        Let the Contender know it hasn't been forgotten
        """
        self.__last_update_ts = int(time.time())

    def make_owner(self):
        """
        Let the Contender know it is now the lock owner
        """
        if not self.__am_owner:
            self.__last_update_ts = int(time.time())
            self.__obtain_ts = int(time.time())
            self.__am_owner = 1

    def is_owner(self):
        """
        Returns whether or not this Contender is actually the owner
        """
        return self.__am_owner

    def stream_out(self):
        """
        Returns a value suitable for sending out a socket
        """
        priority = pack("!B", self.__priority)

        ttl = pack("!H", self.__ttl)

        ttw = pack("!H", self.__ttw)

        index = pack("!B", len(self.__index))
        index += self.__index

        owner = pack("!B", len(self.__owner))
        owner += self.__owner

        am_owner = pack("!B", self.is_owner())

        retval = priority + ttl + ttw + index + owner + am_owner

        return retval

    def deletable(self):
        """
        Returns a boolean indicating if this instance is ready to be deleted
        or not
        """
        if self.is_owner():
            return (int(time.time()) - self.__last_update_ts) > self.__ttl
        else:
            return (int(time.time()) - self.__last_update_ts) > self.__ttw

    def __lt__(self, other):
        """
        Less than comparator
        """
        if type(other) is not Contender:
            return 0

        if self.__index == other.get_index() and \
        self.__key == other.get_key() and \
        self.__owner == other.get_owner() and \
        self.__priority < other.get_priority():
            return 1
        else:
            return 0

    def __gt__(self, other):
        """
        Greater than comparator
        """
        if type(other) is not Contender:
            return 0

        if self.__index == other.get_index() and \
        self.__key == other.get_key() and \
        self.__owner == other.get_owner() and \
        self.__priority > other.get_priority():
            return 1
        else:
            return 0

    def __eq__(self, other):
        """
        Equality comparator
        """
        if type(other) is not Contender:
            return 0

        if self.__index == other.get_index() and \
        self.__key == other.get_key() and \
        self.__owner == other.get_owner():
            return 1
        else:
            return 0

    def __ne__(self, other):
        """
        Inequality comparator
        """
        if type(other) is not Contender:
            return 0

        if self == other:
            return 0
        else:
            return 1

    def __str__(self):
        """
        Stringify
        """
        if self.is_owner():
            seconds = self.__ttl - (int(time.time()) - self.__last_update_ts)
        else:
            seconds = self.__ttw - (int(time.time()) - self.__last_update_ts)

        return "Index: " + self.__index + " " + \
               "Key: " + self.__key + " " + \
               "Owner: " + self.__owner + " " + \
               "Deletable in: " + str(seconds) + " seconds"
