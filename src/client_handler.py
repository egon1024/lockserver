"""
This module encapsulates the class for handling a remote client
"""
from select import select
from struct import pack

import syslog
import threading
import traceback

# 3rd Party imports

# Lockserver imports
from butler import DisconnectedError
import butler


class ClientHandler(threading.Thread):
    """
    This class encapsulates the work for handling a client
    """
    def __init__(
        self, master_keylist, client_socket, roster, accountant, owner_list
    ):
        """
        Initializer.  Expects a toplevel keylist, client socket, Roster,
        accountant, and an owner_list
        """
        self.master_keylist = master_keylist
        self.client_socket = client_socket
        self.accountant = accountant
        self.roster = roster
        self.owner_list = owner_list

        threading.Thread.__init__(self)

    def run(self):
        """
        This is the main execution routine for the thread
        """

        # First thing - we need to welcome the new client
        self.client_socket.send(pack("!B", 1))
        self.accountant.increment("active_connections")

        # We'll need a Butler, let's appropriate one
        jeeves = butler.Butler(
            master_keylist=self.master_keylist,
            roster=self.roster,
            owner_list=self.owner_list,
            accountant=self.accountant,
        )

        readsocks = [self.client_socket]

        while True:
            try:
                readables = select(readsocks, [], [])[0]
                readable_socket = readables[0]
                jeeves.execute_command(readable_socket)
            except DisconnectedError:
                self.accountant.decrement("active_connections")
                self.client_socket.close()
                break
            except:
                trace_str = traceback.format_exc()
                syslog.syslog(syslog.LOG_ERR, trace_str)
                break
