"""
This class encapsulates a list of indexes
"""

# Built in imports
import threading

# 3rd Party imports

# Lockserver imports
from contender_list import ContenderList


class IndexList(object):
    """
    A class to encapsulates lists of indexes
    """
    def __init__(self, owner_list=None):
        """
        Initializer.
        """
        self.p__mutex = threading.Lock()
        self.__contender_lists = {}
        self.__owner_list = owner_list

    def find(self, index):
        """
        Find a ContenderList based off a particular index
        """
        if type(index) != str:
            raise TypeError("Unknown type passed to find()")

        return self.__contender_lists.get(index, None)

    def get_ist(self):
        """
        Get a list of all the contenders
        """
        return self.__contender_lists.values()

    def lock(self):
        """
        Get an exclusive lock on the object
        """
        self.p__mutex.acquire()

    def unlock(self):
        """
        Release a lock on the object
        """
        self.p__mutex.release()

    def setdefault(self, index):
        """
        Define a default index for this list
        """
        tmp = ContenderList(self.__owner_list)
        return self.__contender_lists.setdefault(index, tmp)

    def private_unsafe_clean(self):
        """
        Performs a clean of any data in the contender lists, but in an unsafe
        manner (meaning it does not perform locking on individual resources.
        This is specifically intended for use by a Maid.
        """

        retval = 0

        for (index, contender_list) in self.__contender_lists.items():
            retval += contender_list.private_unsafe_clean()
            if len(contender_list) == 0:
                retval += 1
                del self.__contender_lists[index]

        return retval

    def string_lock_dump(self):
        """
        Gives a string representation of what the list looks like
        """
        retval = []

        for (key, index) in self.__contender_lists.items():
            retval.append("    " + key + ":")
            retval += index.string_lock_dump()

        return retval

    def __len__(self):
        """
        Return a length
        """
        return len(self.__contender_lists)
