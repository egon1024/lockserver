"""
This module contains code to encapsulate the representation of an owner of a
lock request
"""

# Built in imports
import threading

# 3rd Party imports

# Lockserver imports


class OwnerData(object):
    """
    Encapsulates the representation of an owner of a lock request
    """
    def __init__(self, owner=''):
        """
        Initializer.  Expects a string representing the name of the owner.
        """
        self.__owner = owner
        self.__mutex = threading.Lock()
        self.__contenders = {}

    def add(self, contend):
        """
        Adds a contender to the owner
        """

        self.__contenders[contend] = 1

    def remove(self, contend):
        """
        Removes a contender from the owner.  Not explicitly threadsafe.
        """

        if contend in self.__contenders:
            del self.__contenders[contend]

    def get_list(self):
        """
        Get a list of contenders for the owner
        """
        return self.__contenders.keys()

    def __len__(self):
        """
        Determine the length of the object
        """
        return len(self.__contenders)

    def lock(self):
        """
        Lock the object.
        """
        self.__mutex.acquire()

    def unlock(self):
        """
        Unlock the object.
        """
        self.__mutex.release()

    def string_lock_dump(self):
        """
        Gives a string representation of what the list looks like
        """
        retval = []
        for key in self.__contenders.keys():
            retval.append("    (%s, %s)" % (key.get_key(), key.get_index()))

        return retval
